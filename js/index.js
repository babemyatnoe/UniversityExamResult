(function($){
	function floatLabel(inputType){
		$(inputType).each(function(){
			var $this = $(this);
			// on focus add cladd active to label
			$this.focus(function(){
				$this.next().addClass("active");
			});
			//on blur check field and remove class if needed
			$this.blur(function(){
				if($this.val() === '' || $this.val() === 'blank'){
					$this.next().removeClass();
				}
			});
		});
	}
	// just add a class of "floatLabel to the input field!"
	floatLabel(".floatLabel");

	function updateTotal() {
		var price = parseFloat($("#price").val());
		var qty = parseFloat($("#qty").val());
		var total = price * qty;
	
		if (!isNaN(total)) {
			$("#total").val(total);
		}
	}	

	$("#qty").on("change paste keyup", function() {
		updateTotal();
	});

	$("#price").on("change paste keyup", function() {
		updateTotal();
 	});

})(jQuery);