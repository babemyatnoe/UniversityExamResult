<?php
	// require_once "./../models/mysql_connection.php";
	require_once "app/models/mysql_connection.php";

  if (isset($_POST["refresh"])) {
    echo '<div class="col-sm-12">';
		$stmt = $connection->prepare("SELECT * FROM transaction");
		$stmt->execute();
		$res = $stmt->get_result();
    $row = $res->fetch_assoc();

    while ($row = $res->fetch_assoc()) {
      $id = $row["id"];
      $item = $row["item"];
      $price = $row["price"];
      $qty = $row["qty"];
      $total = $row["total"];
      $cashier = $row["cashier"];
      $prevHash = $row["prevHash"];
      $currentHash = $row["hash"];
      
      echo '<div class="col-sm-3">';

      $hash = hash('sha256', $id . $item . $price . $qty . $total . $cashier . $prevHash);
      if ($currentHash == $hash) {
        echo '<div class="panel panel-success"><div class="panel-heading"><h4 class="panel-title">';
        echo "VERIFIED";
      } else {
        echo '<div class="panel panel-danger"><div class="panel-heading"><h4 class="panel-title">';
        echo "CORRUPTED";
      }
 
      echo '</h4></div><div class="panel-wrapper collapse in"><div class="panel-body">';
      echo '<p>Item: ' . $item . '</p>';
      echo '<p>Price: ' . $price . '</p>';
      echo '<p>Qty: ' . $qty . '</p>';
      echo '<p>Total: ' . $total . '</p>';
      echo '<p>Cashier: ' . $cashier . '</p>';
      echo '</div> </div> </div> </div>';
      
   }

		/* free results */
		$stmt->free_result();
		/* close statement */
		$stmt->close();

    echo "</div>";
		// $hash = hash('sha256', $id . $item . $price . $qty . $total . $cashier . $prevHash);

  }
	
?>
