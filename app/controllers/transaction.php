<?php
	require_once "app/models/mysql_connection.php";

  if (isset($_POST["confirm"])) {
		$item = $_POST["item"];
		$price = $_POST["price"];
		$qty = $_POST["qty"];
		$total = $_POST["total"];
		$cashier = $_POST["cashier"];

		$stmt = $connection->prepare("SELECT * FROM transaction ORDER BY id DESC LIMIT 1");
		$stmt->execute();
		$res = $stmt->get_result();
		$row = $res->fetch_assoc();
		
		$id = $row["id"] + 1;
		$prevHash =  $row["hash"];
	
		/* free results */
		$stmt->free_result();
		/* close statement */
		$stmt->close();

		echo( $id . $item . $price . $qty . $total . $cashier . $prevHash);
		$hash = hash('sha256', $id . $item . $price . $qty . $total . $cashier . $prevHash);

		$query = "INSERT INTO transaction (item, price, qty, total, cashier, prevHash, hash)"
			. " VALUES (?, ?, ?, ?, ?, ?, ?)";
		$statement = $connection ->prepare($query);
		$statement ->bind_param("siiisss", $item, $price, $qty, $total, $cashier, $prevHash, $hash);
		$statement ->execute();

		if($statement->error) {
			$err = $statement->error;
			echo $err;
		}
		else {
			echo "SUCCESS";
		}
		$statement ->close();
  }
	
?>
