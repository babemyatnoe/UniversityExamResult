<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Basic hotel booking form</title>
  <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
<link href='https://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'>
  
  
      <link rel="stylesheet" href="css/style.css">

  
</head>

<body>
<?php
  include("./app/controllers/transaction.php");
?>
<div class="head"><h1>Student Mark Form</h1></div>
<div class="sky">
    <div class="moon"></div>
    <div class="clouds_two"></div>
    <div class="clouds_one"></div>
    <div class="clouds_three"></div>
    <form action="" method="POST">
  <!--  General -->
<div class="form-group">
    <div class="col-1-2 m-b-20">
      <div class="controls">
        <input type="text" id="item" class="floatLabel" name="item" required>
        <label for="item">Item</label>
      </div>
      <div class="controls">
        <input type="number" id="price" class="floatLabel" name="price" required>
        <label for="price">Price</label>
      </div>       
      <div class="controls">
        <input type="number" id="qty" class="floatLabel" name="qty" required>
        <label for="qty">Qty</label>
      </div>
      <div class="controls">
        <input type="number" id="total" class="floatLabel" name="total" readonly>
        <label class="active" for="total">Total</label>
      </div>
      <div class="controls">
        <i class="fa fa-sort"></i>
        <select id="cashier" name="cashier" class="floatLabel">
          <option value="judy" selected>Judy</option>
          <option value="cathy">Cathy</option>
          <option value="rick">Rick</option>
        </select>
        <label for="cashier"><i class="fa fa-male"></i>&nbsp;&nbsp;Cashier</label>
      </div>
      <button name="confirm" type="submit" value="Submit" class="col-1-4 c-white">Confirm</button>
    </div>
</form>
</div>


<?php include("app/views/verification.php"); ?>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<!-- <script src='http://raw.githubusercontent.com/andiio/selectToAutocomplete/master/jquery-ui-autocomplete.js'></script>
<script src='http://raw.githubusercontent.com/andiio/selectToAutocomplete/master/jquery.select-to-autocomplete.js'></script>
<script src='http://raw.githubusercontent.com/andiio/selectToAutocomplete/master/jquery.select-to-autocomplete.min.js'></script> -->
  <script  src="js/index.js"></script>

</body>

</html>
